import subprocess
from time import sleep

from selenium import webdriver

torexe = subprocess.Popen(r"tor-win32-0.4.4.5\Tor\tor.exe")

PROXY = "socks5://localhost:9050"  # IP:PORT or HOST:PORT
options = webdriver.ChromeOptions()
options.add_argument('--proxy-server=%s' % PROXY)
binary = r'chromedriver.exe'
driver = webdriver.Chrome(options=options, executable_path=binary)
driver.get("https://check.torproject.org/")
sleep(3)
driver.quit()
torexe.terminate()
